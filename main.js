require('dotenv').config()

const puppeteer = require('puppeteer');

const degiroService = require("./degiro/degiroService");
const tvService = require("./tradingview/tvService");


async function start() {
    global.browser = await puppeteer.launch({ headless: false, defaultViewport: null, });

    let defaultStock = [];

    await degiroService.authent();
    defaultStock = await degiroService.extractDegiroData();

    await tvService.authent();
    await tvService.insertStocks(defaultStock);
    console.log("Program finished")
}

start()