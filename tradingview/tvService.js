module.exports = { authent,insertStocks };
const tvAuthent = require("./tvAuthent");
const tvProvider = require("./tvProvider");


async function authent(){
    await tvAuthent.doLogin();
}

async function insertStocks(stocks){
    await tvProvider.insertAllStocks(stocks);
}