module.exports = { insertAllStocks };

async function insertAllStocks(stocks){
    let chart_url = process.env.TRADINGVIEW_CHART_URL;

    let page = await global.browser.newPage();
    await page.goto(chart_url);

    await page.waitForSelector('.widgetbar-pages', {visible: true,timeout: 10000 })
    await removeOldStock(page);
    await initInsertStocks(page,stocks)
    await page.close();

}

async function removeOldStock(page){
    await page.$eval('div[data-name="watchlists-button"]',el => el.click())
    await page.waitForSelector('div[data-name="menu-inner"] div[class*="item-"]', {visible: true,timeout: 5000 })
    await page.waitForTimeout(1000);

    let foundedList = await page.$$('div[data-name="menu-inner"] div[class*="item-"]');
    let targetList = await findElementFromListWithText(foundedList,"Red list");
    await targetList.evaluate(it => it.click());
    
    await page.waitForTimeout(1000);
    await page.$eval('div[data-name="settings-button"]',el => el.click())
    await page.waitForTimeout(1000);

    let allOption = await page.$$('div[data-name="menu-inner"] div[class*="item-"]');
    let targetOption = await findElementFromListWithText(allOption,"Clear list");
    await targetOption.evaluate(it => it.click());
    await page.waitForTimeout(1000);


    await page.$eval('button[name="yes"]',el => el.click())

}

async function initInsertStocks(page,stocks){
    await page.waitForSelector('div[data-name="add-symbol-button"]', {visible: true,timeout: 10000 })

    await page.$eval('div[data-name="add-symbol-button"]',el => el.click())
    await page.waitForSelector('input[data-role="search"]', {visible: true,timeout: 5000 })

    for(let stockid of stocks){
        await insertStock(page,stockid)
    }
}

async function insertStock(page,stockid){


    await page.$eval('input[data-role="search"]', (el,sid) => el.value= sid, stockid)
    await page.$eval('input[data-role="search"]', el => el.dispatchEvent(new Event('input')))
    await page.waitForTimeout(1000);

    await page.$eval('[class*="markedFlag-"]', el => el.click())
    await page.waitForTimeout(500);
    console.log(`Added stock ${stockid} to tv`)

}

async function findElementFromListWithText(elements, searchTxt){
    for(let list of elements){
        let text = await list.evaluate(it =>  it.innerText)
        if(text.includes(searchTxt)){
            return list;
        }
    }

    for(let list of elements){
        let text = await list.evaluate(it =>  it.innerText)
        console.error("Matching text: "+text)
    }
    throw new Error("Failed to find element from list "+searchTxt);
}


