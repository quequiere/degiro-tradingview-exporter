module.exports = { doLogin };

async function doLogin(){
    let username = process.env.TRADINGVIEW_USERNAME;
    let password = process.env.TRADINGVIEW_PASSWORD;
    let login_url = process.env.TRADINGVIEW_LOGIN_URL;

    let page = await global.browser.newPage();
    await page.goto(login_url);

    await page.$eval('button[aria-label="Open user menu"]',el => el.click());

    await page.waitForSelector('div[data-name="header-user-menu-sign-in"]', {visible: true,timeout: 10000 })
    await page.$eval('div[data-name="header-user-menu-sign-in"]',el => el.click());

    await page.waitForSelector('.i-clearfix span', {visible: true,timeout: 10000 })
    await page.$eval('.i-clearfix span',el => el.click());



    await page.waitForSelector('input[name="username"]', {visible: true,timeout: 10000 })

    await page.$eval('input[name="username"]',(el,uname) => el.value = uname,username);
    await page.$eval('input[name="password"]',(el,pwd) => el.value = pwd,password);

    await page.$eval('button[type="submit"]',el => el.click());


    await page.waitForTimeout(3000);
    await page.close();

}