module.exports = { extractPortfolio };

async function extractPortfolio(){
    let portfolio_url = process.env.DEGIRO_PORTFOLIO_URL;

    let page = await global.browser.newPage();
    await page.goto(portfolio_url);

    let tvstock = await scanAllRows(page);

    await page.close();

    return tvstock;

}

async function scanAllRows(page){

    console.log("Start scan stock rows")


    let cssTRSelector = 'div[data-name="positions"] section:not([data-name="cashFundCompensations"]) tbody tr';

    await page.waitForSelector(cssTRSelector, {visible: true,timeout: 10000 })
    let rows = await page.$$(cssTRSelector)

    let results = [];

    for(let row of rows){
       let tvstock = await evaluateRow(page, row)
       if(tvstock!=undefined){
            results.push(tvstock.trim());
       }
    }

    console.log("Scan is finished")
    return results;
}

async function evaluateRow(page, row){
    await row.evaluate(it => it.click());

    let dataText = await page.$eval('span[data-name="productBriefInfo"]', el => el.innerText)
    let splitedText = dataText.split("|");

    if(splitedText.length<3){
        console.error("Failed to analyse stock, not enough data: "+dataText);
        return;
    }

    let tradingViewExchange = exchangeConvert(splitedText[2].trim())
    if(!tradingViewExchange){
        console.error("Failed to get exchange tradingview "+splitedText[2].trim());
        return;
    }

    let finalExtractionName = `${tradingViewExchange}:${splitedText[0]}`;

    //Trick to close details with different screen format
    try{
        await page.$eval('button[data-name="sideInformationPanelBackButton"]', it => it.click());
    }catch(exception){

    }

    try{
        await page.$eval('i[data-type="close"]', it => it.click());
    }catch(exception){
        
    }

    console.log(`Extracted: ${finalExtractionName}`)

    return finalExtractionName;
}

function exchangeConvert(marketplaceDegiro){
    if(marketplaceDegiro == "Euronext Paris"){
        return "EURONEXT"
    }else if(marketplaceDegiro == "New York Stock Exchange"){
        return "NYSE"
    }else if(marketplaceDegiro == "NASDAQ"){
        return "NASDAQ"
    } else if(marketplaceDegiro == "Borse Frankfurt"){
        return "FWB"
    }else if(marketplaceDegiro == "Borse Frankfurt"){
        return "FWB"
    }

    return null;

}

