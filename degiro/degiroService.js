module.exports = { extractDegiroData, authent };
const degiroAuthent = require("./degiroAuthent");
const degiroProvider = require("./degiroProvider");


async function authent(){
    await degiroAuthent.doLogin();
}

async function extractDegiroData(){
    let tvStock = await degiroProvider.extractPortfolio();
    console.log(`Extracted ${tvStock.length} stock data`)
    return tvStock;
}