module.exports = { doLogin };

async function doLogin(){
    let username = process.env.DEGIRO_USERNAME;
    let password = process.env.DEGIRO_PASSWORD;
    let login_url = process.env.DEGIRO_LOGIN_URL;

    let page = await global.browser.newPage();
    await page.goto(login_url);

    await page.$eval("#username",(el,uname) => el.value = uname,username);
    await page.$eval("#password",(el,pswrd) => el.value = pswrd,password);
    await page.$eval('button[name="loginButtonUniversal"]',el => el.click());

    await page.waitForNavigation();
    await page.close();

}